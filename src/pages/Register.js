import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext.js';

export default function Register(){

	const {user} = useContext(UserContext)

	// State Hooks -> store values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');


	const [isActive, setIsActive] = useState(false);

/*	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(password1);
	console.log(password2);*/

	useEffect(() => {
		// Validation to enable register button when all fields are populated and both password match
		if((firstName !== '' && lastName !=='' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	});
function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
			})
		})
    	.then(res => res.json())
    	.then(data => {

	    	// console.log(data);

    		if(data) {

				Swal.fire({
					title: 'Registration Failed',
					icon: 'error',
					text: 'Email already exists. Please try again!'
				})
			}
			else {

				fetch('http://localhost:4000/users/register', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
					body: JSON.stringify({
						firstName : firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNumber,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)

					Swal.fire({
						title: 'Registration Successful',
						icon: 'success',
						text: 'Welcome to Zuitt!'
					})

					navigate('/login');
		    	})
				
			}
		})

	// Function to simulate user registration

	function registerUser(e){
		// prevents page redirection via form submission
		e.preventDefault();

		setFirstName ('');
		setLastName ('');
		setEmail('');
		setPassword1('');
		setPassword2('');

		alert("Thank you for registering!");
	}


	return(
			(user.id !== null)? <Navigate to ="/courses" /> :

	<Form onSubmit={(e) => registerUser(e)}>

			(user.id !== null) ? <Navigate to="/courses"/> :
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group className="mb-3" controlId="userFistName">
			    	<Form.Label>First Name</Form.Label>
			        <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="userLastName">
			    	<Form.Label>Last Name</Form.Label>
			        <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
			    </Form.Group>


      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1" >
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2" >
        <Form.Label>Verify Password</Form.Label>
        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
      </Form.Group>

      {/*CONDITIONAL RENDERING -> IF ACTIVE BUTTON IS CLICKBLE -> IF INACTIVE BUTTON IS NOT CLIC*/}
      {
      	(isActive) ?
      	<Button variant="primary" type="submit" controlId="submitBtn">
        Register
      	</Button>
      	:
      	<Button variant="primary" type="submit" controlId="submitBtn" disabled>
        Register
      	</Button>
      }

      
      
    </Form>
		)
}